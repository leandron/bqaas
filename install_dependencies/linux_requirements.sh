# system 
sudo apt-get update
sudo apt-get -y dist-upgrade
sudo apt-get -y install linux-headers-$(uname -r) virtualbox-guest-utils swig g++ uuid-dev make

# tools
sudo apt-get -y install vim python2.7-dev python-pip libevent-dev libtool autoconf automake uuid-dev build-essential

# web
sudo apt-get -y install nginx curl apache2-utils

#ZMQ
wget http://download.zeromq.org/zeromq-4.0.4.tar.gz
tar xzvf zeromq-4.0.4.tar.gz
cd zeromq-4.0.4/
./configure
make
sudo make install
cd ..