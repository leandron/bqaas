from flask import Flask, jsonify, g, abort, render_template, request

import sqlite3

DATABASE_PATH = '/tmp/quotes.db'

app = Flask(__name__)

# Database -----

def connect_db():
    return sqlite3.connect(DATABASE_PATH)

@app.before_request
def before_request():
    g._database = connect_db()

def query_db(query, args=(), one=False):
    cur = g._database.execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

# Routes -------

@app.route("/")
def list_quotes():
    quotes = query_db('select id, author, quote from Quotes order by author')
    return render_template('quote_list.html', quotes=quotes)


@app.route("/quotes", methods = ['GET'])
def get_quotes():
    quotes = query_db('select id, author, quote from Quotes order by author')
    return jsonify(list((row[0], { 'author' : row[1], 'quote' :row[2] }) for row in quotes))


@app.route("/quotes/<int:quote_id>", methods = ['GET'])
def get_quote(quote_id):
    quotes = query_db('select id, author, quote from Quotes where id = ?',[quote_id])
    if len(quotes) == 0:
        abort(404)
    return jsonify(list((row[0], { 'author' : row[1], 'quote' :row[2] }) for row in quotes))


@app.route("/quotes", methods = ['POST'])
def post_quote():
    if not request.json or not 'author' in request.json or not 'quote' in request.json:
        abort(400)

    new_quote = query_db('INSERT INTO Quotes(author, quote) VALUES(?,?)',
        [request.json['author'],request.json['quote']])
    g._database.commit()
    return jsonify( { 'status': 'New quote created.' } ), 201


@app.route("/quotes/<int:quote_id>", methods = ['DELETE'])
def delete_quote(quote_id):
    quotes = query_db('select id, author, quote from Quotes where id = ?',[quote_id])
    if len(quotes) == 0:
        abort(404)

    query_db('delete from Quotes where id = ?',[quote_id])
    g._database.commit()
    return jsonify( { 'status': 'Quote deleted.' } ), 200

if __name__ == "__main__":
    app.run(debug=True)