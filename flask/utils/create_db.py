#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys

con = lite.connect('/tmp/quotes.db')

with con:
    
    cur = con.cursor()    
    cur.execute("CREATE TABLE Quotes(id INTEGER PRIMARY KEY, author TEXT, quote TEXT)")
    cur.execute("INSERT INTO Quotes(author, quote) VALUES('Seu Madruga','A Vingança nunca é plena, mata a alma e a envenena')")
    cur.execute("INSERT INTO Quotes(author, quote) VALUES('Chaves','As pessoas boas devem amar seus inimigos')")
    cur.execute("INSERT INTO Quotes(author, quote) VALUES('Bruxa Maratuxa','Parangaricutirimirruaru!')")


